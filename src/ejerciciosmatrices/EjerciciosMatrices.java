/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosmatrices;

import matriz.Matriz;

/**
 *
 * @author nachorod
 */
public class EjerciciosMatrices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Matriz m1= new Matriz(5);
        m1.mostrar();
        Matriz m2= new Matriz(3,4);
        m2.mostrar();
        
    } // Fin main
    
    
} //Fin clase
